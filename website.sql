-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-06-2024 a las 04:44:01
-- Versión del servidor: 10.4.28-MariaDB
-- Versión de PHP: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `website`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_configuraciones`
--

CREATE TABLE `tbl_configuraciones` (
  `id` int(11) NOT NULL,
  `nombreconfiguracion` varchar(255) NOT NULL,
  `valor` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `tbl_configuraciones`
--

INSERT INTO `tbl_configuraciones` (`id`, `nombreconfiguracion`, `valor`) VALUES
(1, 'bienvenidad_principal_1', 'bienvenid@ a tu espacio'),
(2, 'bienvenidad_secundaria', 'ungusto conocerte'),
(3, 'boton_principal', 'inicio'),
(4, 'link_boton_principal', '#servicios');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_entradas`
--

CREATE TABLE `tbl_entradas` (
  `id` int(11) NOT NULL,
  `fecha` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `imagen` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `tbl_entradas`
--

INSERT INTO `tbl_entradas` (`id`, `fecha`, `titulo`, `descripcion`, `imagen`) VALUES
(1, '2023-05-28', '        teste2022', 'qewrtafgh', '1686512071_5d427963a7d9e8c3bc59878b06d51ac5.jpg'),
(2, '2024-04-11', ' teste2022', 'En un mundo lleno de posibilidades, cada día nos enfrentamos a desafíos y oportunidades que nos invitan a explorar nuevas perspectivas. La vida es como un lienzo en blanco esperando a ser llenado con colores vibrantes y emociones profundas. Nos encontramo', '1686540640_descarga.jpg'),
(3, '2027-06-30', 'testandoooooo', 'Recuerda que cada día es una oportunidad para escribir una nueva página en tu libro personal. Atrévete a soñar en grande, a perseguir tus metas con determinación y a abrazar los cambios como oportunidades de crecimiento. No temas a los desafíos, abraza la', '1686540851_bb6bb7355efe1e2b61f48f165b3138e2.png'),
(4, '2029-06-06', 'Santos', 'En un mundo lleno de posibilidades, cada día nos enfrentamos a desafíos y oportunidades que nos invitan a explorar nuevas perspectivas. La vida es como un lienzo en blanco esperando a ser llenado con colores vibrantes y emociones profundas. Nos encontramo', '1686540875_f7266b3f8e4f419ee89611f879b7ece3--cat-eye-glasses-wearing-glasses.jpg'),
(5, '2024-01-03', 'Santos', 'Recuerda que cada día es una oportunidad para escribir una nueva página en tu libro personal. Atrévete a soñar en grande, a perseguir tus metas con determinación y a abrazar los cambios como oportunidades de crecimiento. No temas a los desafíos, abraza la', '1705181577_gato1.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_equipo`
--

CREATE TABLE `tbl_equipo` (
  `id` int(11) NOT NULL,
  `imagen` varchar(255) NOT NULL,
  `nombrecompleto` varchar(255) NOT NULL,
  `puesto` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `linkedin` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `tbl_equipo`
--

INSERT INTO `tbl_equipo` (`id`, `imagen`, `nombrecompleto`, `puesto`, `twitter`, `facebook`, `linkedin`) VALUES
(1, '1705200863_gato4.jpg', 'gato testando', 'gerente test', 'https://twitter.com/g0nzanotes', 'https://twitter.com/g0nzanotes', 'https://twitter.com/g0nzanotes'),
(2, '1705192257_gato2.jpg', 'gato test', 'gerente test', 'https://twitter.com/g0nzanotes', 'https://twitter.com/g0nzanotes', 'https://twitter.com/g0nzanotes'),
(3, '1705192265_gato3.png', 'gato testaaand0', 'gerente test', 'https://twitter.com/g0nzanotes', 'https://twitter.com/g0nzanotes', 'https://twitter.com/g0nzanotes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_portafolio`
--

CREATE TABLE `tbl_portafolio` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `subtitulo` varchar(255) NOT NULL,
  `imagen` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `cliente` varchar(255) NOT NULL,
  `categoria` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `tbl_portafolio`
--

INSERT INTO `tbl_portafolio` (`id`, `titulo`, `subtitulo`, `imagen`, `descripcion`, `cliente`, `categoria`, `url`) VALUES
(1, '       PROJECT NAME hola', '       Lorem ipsum dolor sit amet consectetur.', '1703623341_f7266b3f8e4f419ee89611f879b7ece3--cat-eye-glasses-wearing-glasses.jpg', '       Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, ', '       Threads hola', '       Illustration', '       http://localhost/website/'),
(2, 'PROJECT NAME test', 'Lorem ipsum dolor', '1680807248_2.jpg', 'Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostru', 'Explore', 'Graphic Design', 'http://localhost/website/'),
(3, 'PROJECT NAME', 'Lorem ipsum dolor sit amet consectetur.', '1680807294_3.jpg', 'Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum', 'Finish', 'Identity', 'http://localhost/website/'),
(4, 'PROJECT NAME', 'Lorem ipsum dolor sit amet consectetur.', '1680807352_4.jpg', 'Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum', 'Lines', 'Branding', 'http://localhost/website/'),
(5, 'PROJECT NAME', 'Lorem ipsum dolor sit amet consectetur.', '1680807564_5.jpg', 'Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum', 'Southwest', 'Website Design', 'http://localhost/website/'),
(6, 'PROJECT NAME', 'Lorem ipsum dolor sit amet consectetur.', '1680807605_6.jpg', 'Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostru', 'Window', 'fotografia', 'http://localhost/website/');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_servicios`
--

CREATE TABLE `tbl_servicios` (
  `id` int(11) NOT NULL,
  `icono` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `tbl_servicios`
--

INSERT INTO `tbl_servicios` (`id`, `icono`, `titulo`, `descripcion`) VALUES
(1, 'fa-users', 'dilma test', 'sistemas a medidas hechas para tiendas pequeñas a solo firma y a coutas'),
(2, 'fa-laptop', 'TEST', 'Los mejores profesionales en la industria de equipos'),
(3, 'fa-lock', 'Seguridad Zxzxsz', 'Damos todo tipo de asesoria en seguridad para su pequeña o grande empresa ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_usuarios`
--

CREATE TABLE `tbl_usuarios` (
  `id` int(11) NOT NULL,
  `usuario` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `correo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `tbl_usuarios`
--

INSERT INTO `tbl_usuarios` (`id`, `usuario`, `password`, `correo`) VALUES
(2, 'admin', '1234', 'admin@admin');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_configuraciones`
--
ALTER TABLE `tbl_configuraciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_entradas`
--
ALTER TABLE `tbl_entradas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_equipo`
--
ALTER TABLE `tbl_equipo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_portafolio`
--
ALTER TABLE `tbl_portafolio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_servicios`
--
ALTER TABLE `tbl_servicios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbl_configuraciones`
--
ALTER TABLE `tbl_configuraciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tbl_entradas`
--
ALTER TABLE `tbl_entradas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tbl_equipo`
--
ALTER TABLE `tbl_equipo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tbl_portafolio`
--
ALTER TABLE `tbl_portafolio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `tbl_servicios`
--
ALTER TABLE `tbl_servicios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
