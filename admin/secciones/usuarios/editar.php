<?php
include ("../../bd.php");

if(isset($_GET['txtid'])){
  //editar y recuperar seleccionado
  //echo $_GET['txtid'];
  
  $txtid=(isset($_GET['txtid']) )? $_GET['txtid']:"";
  
  $sentencia=$conexion->prepare(" SELECT * FROM tbl_usuarios WHERE id=:id ");
  $sentencia->bindParam(":id",$txtid);
  $sentencia->execute();
  $registro=$sentencia->fetch(PDO::FETCH_LAZY);

  $usuario=$registro['usuario'];
  $password=$registro['password'];
  $correo=$registro['correo'];

  }

  
  if($_POST){
    //print_r($_POST);

    //update del servicios
    $txtid=(isset($_POST['id']))?$_POST['id']:"";
    $usuario=(isset($_POST['usuario']))?$_POST['usuario']:"";
    $password=(isset($_POST['password']))?$_POST['password']:"";
    $correo=(isset($_POST['correo']))?$_POST['correo']:"";

   // echo $icono; para testar usar echo
   $sentencia=$conexion->prepare("UPDATE tbl_usuarios 
   SET 
   usuario=:usuario,
   password=:password,
   correo=:correo
   WHERE id=:id" );

    $sentencia->bindParam(":usuario",$usuario);
    $sentencia->bindParam(":password",$password);
    $sentencia->bindParam(":correo",$correo);
    $sentencia->bindParam(":id",$txtid);
    $sentencia->execute();
   $mensaje="registro de servicio actualizado con exito.";
   header("Location:index.php?mensaje=".$mensaje);
  }




include("../../templates/headear.php");?>

<div class="card">
  <div class="card-header">Usuario</div>
  <div class="card-body">

    <form action="" method="post">
    <div class="mb-3">
      <label for="id" class="form-label">ID</label>
      <input readonly value="<?php echo $txtid;?>" type="text"
        class="form-control" name="id" id="id" aria-describedby="helpId" placeholder="ID">
    </div>
      <div class="mb-3">
        <label for="" class="form-label">Nombre del usuario:</label>
        <input
          type="text" value="<?php echo $usuario;?>"
          class="form-control"
          name="usuario"
          id="usuario"
          aria-describedby="helpId"
          placeholder="" /></div>
          <div class="mb-3">
            <label for="" class="form-label">Password:</label>
            <input
              type="password" value="<?php echo $password;?>"
              class="form-control"
              name="password"
              id="password"
              aria-describedby="helpId"
              placeholder="password" /></div>
              <div class="mb-3">
                <label for="" class="form-label">Correo:</label>
                <input
                  type="email" value="<?php echo $correo;?>"
                  class="form-control"
                  name="correo"
                  id="correo"
                  aria-describedby="helpId"
                  placeholder=""/></div>
                  <button type="submit" class="btn btn-success">Agregar</button>

                    <a name="" id="" class="btn btn-primary" href="index.php" role="button">Cancelar</a>

          
      
    </form>
  </div>
</div>
<?php include("../../templates/footer.php");?>