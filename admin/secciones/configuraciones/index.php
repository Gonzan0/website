<?php
 include ("../../bd.php");
 if(isset($_GET['txtid'])){
  //borrar
  //echo $_GET['txtid']; para ver si funciona
  
  $txtid=(isset($_GET['txtid']) )? $_GET['txtid']:"";
  
  $sentencia=$conexion->prepare(" DELETE FROM tbl_configuraciones WHERE id=:id ");
  $sentencia->bindParam(":id",$txtid);
  $sentencia->execute();
  
  }


 //selecionar registros
$sentencia=$conexion->prepare("SELECT * FROM `tbl_configuraciones`");
$sentencia->execute();
$lista_configuraciones=$sentencia->fetchAll(PDO::FETCH_ASSOC);


 include("../../templates/headear.php");?>

<div class="card">
<div class="card-header">  <a name="" id="" class="btn btn-primary" href="crear.php" role="button">Agregar Usuario</a></div>

  <div class="card-body">
   <div
    class="table-responsive-sm">
    <table
      class="table">
      <thead>
        <tr>
          <th scope="col">Id</th>
          <th scope="col">Nombre de la configuracion</th>
          <th scope="col">Valor</th>
          <th scope="col">Acciones</th>
        </tr>
      </thead>
      <tbody>
      <?php foreach($lista_configuraciones as $registros) { ?>
        <tr class="">
          <td><?php echo $registros['id'] ?></td>
          <td><?php echo $registros['nombreconfiguracion'] ?></td>
          <td><?php echo $registros['valor'] ?></td>
          <td>
            <a name="" id="" class="btn btn-info" href="editar.php?txtid=<?php echo $registros['id'] ?>" role="button">Editar</a>
            <!-- |
           <a name="" id="" class="btn btn-danger" href="index.php?txtid=" role="button">Eliminar</a> -->
          </td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
   </div>
   
  </div>

</div>






<?php include("../../templates/footer.php");?>
