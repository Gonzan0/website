<?php 
 include ("../../bd.php");

 if(isset($_GET['txtid'])){
  //editar y recuperar seleccionado
  //echo $_GET['txtid'];
  
  $txtid=(isset($_GET['txtid']) )? $_GET['txtid']:"";
  
  $sentencia=$conexion->prepare(" SELECT * FROM tbl_configuraciones WHERE id=:id ");
  $sentencia->bindParam(":id",$txtid);
  $sentencia->execute();
  $registro=$sentencia->fetch(PDO::FETCH_LAZY);

  $nombreconfiguracion=$registro['nombreconfiguracion'];
  $valor=$registro['valor'];

  }

  if($_POST){
    //print_r($_POST);

    //update del servicios
    $txtid=(isset($_POST['id']))?$_POST['id']:"";
    $nombreconfiguracion=(isset($_POST['nombreconfiguracion']))?$_POST['nombreconfiguracion']:"";
    $valor=(isset($_POST['valor']))?$_POST['valor']:"";

   // echo $icono; para testar usar echo
   $sentencia=$conexion->prepare("UPDATE tbl_configuraciones 
   SET 
   nombreconfiguracion=:nombreconfiguracion,
   valor=:valor
   WHERE id=:id" );

   $sentencia->bindParam(":nombreconfiguracion",$nombreconfiguracion);
   $sentencia->bindParam(":valor",$valor);
   $sentencia->bindParam(":id",$txtid);
   $sentencia->execute();
   $mensaje="registro de servicio actualizado con exito.";
   header("Location:index.php?mensaje=".$mensaje);
  }




include("../../templates/headear.php");?>
<div class="card">
  <div class="card-header">Configuracion</div>
  <div class="card-body">
    
  <form action="" method="post">
  <div class="mb-3">
      <label for="id" class="form-label">id:</label>
      <input value="<?php echo $txtid;?>" 
        type="text"
        class="form-control"
        name="id"
        id="id"
        aria-describedby="helpId"
        placeholder=""
      />
    </div>
    <div class="mb-3">
      <label for="nombreconfiguracion" class="form-label">Nombre de la Configuracion:</label>
      <input value="<?php echo $nombreconfiguracion;?>" 
        type="text"
        class="form-control"
        name="nombreconfiguracion"
        id="nombreconfiguracion"
        aria-describedby="helpId"
        placeholder=""
      />
    </div>
    <div class="mb-3">
      <label for="valor" class="form-label">Valor:</label>
      <input value="<?php echo $valor;?>" 
        type="text"
        class="form-control"
        name="valor"
        id="valor"
        aria-describedby="helpId"
        placeholder=""
      />
    </div>
    <button type="submit" class="btn btn-success">Actualizar</button>

<a name="" id="" class="btn btn-primary" href="index.php" role="button">Cancelar</a>
    

  </form>
  </div>
</div>


<?php include("../../templates/footer.php");?>